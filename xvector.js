/*
 * Description:
 * XVector adds Three-Dimensional Vectors and Matrices.
 * The purpose of this library is to add simple vector support to facilitate
 * quick prototyping of web-based graphical applications.
 *
 * Author: 
 * Mahlon Winstead
 * mwinstead49@gmail.com
 * 8/2017
 */

(function(root){
  /* MATRIX -----------------------------------------------------*/
  function Matrix3(v){
  	this.value = v || [
    	[1, 0, 0],
      [0, 1, 0],
      [0, 0, 1]
    ];
  }
  
  Matrix3.prototype.multiplyMatrix = function(m){
  	var result = new Matrix3();
  	
  	for(var r = 0; r < 3; r++){
    	var row = this.getRowVector(r);
    	for(var c = 0; c < 3; c++){
      	var column = m.getColumnVector(c);
        result.value[r][c] = row.dotProduct(column);
      }
    }
    
    return result;
  }
  
  Matrix3.prototype.addMatrix = function(m){
    return new Matrix3(
      [
				[this.value[0][0]+m.value[0][0], this.value[0][1]+m.value[0][1], this.value[0][2]+m.value[0][2]],
        [this.value[1][0]+m.value[1][0], this.value[1][1]+m.value[1][1], this.value[1][2]+m.value[1][2]],
        [this.value[2][0]+m.value[2][0], this.value[2][1]+m.value[2][1], this.value[2][2]+m.value[2][2]]
      ]
     )
  }
  
  Matrix3.prototype.getRowVector = function(n){
  	var row = this.value[n];
    return new Vec3(row[0], row[1], row[2]);
  }
  
  Matrix3.prototype.getColumnVector = function(n){
    var column = [this.value[0][n], this.value[1][n], this.value[2][n]];
    return new Vec3(column[0], column[1], column[2]);
  }
  
  /* VECTOR -------------------------------------------------------------------*/
  function Vec3(x, y, z){
  	this.x = x;
    this.y = y;
    this.z = z != null ? z : 1;
  }
  
  Vec3.prototype.angle = function(){
  	return Math.atan2(this.y, this.x);
  }
  
  Vec3.prototype.magnitude = function(){
  	return Math.sqrt((this.x * this.x) + (this.y * this.y));
  }
  
  Vec3.prototype.addVector2 = function(b){
  	var x = this.x + b.x;
    var y = this.y + b.y;
    return new Vec3(x, y);
  }
  
  Vec3.prototype.addVector3 = function(b){
	var x = this.x + b.x;
    var y = this.y + b.y;
	var z = this.z + b.z;
	return new Vec3(x, y, z);
  }
  
  Vec3.prototype.subtractVector = function(b){
  	return new Vec3(this.x - b.x, this.y - b.y);
  }
  
  Vec3.prototype.divide = function(a){
  	var x = this.x / a;
    var y = this.y / a;
    return new Vec3(x, y);
  }
  
  Vec3.prototype.scale = function(a){
    return new Vec3(this.x*a, this.y*a);
  }
  
  Vec3.prototype.normalize =function(){
  	var mag = this.magnitude();
    return this.divide(mag);
  }
  
  Vec3.prototype.dotProduct = function(b){
  	return (this.x * b.x) + (this.y * b.y) + (this.z * b.z);
  }
  
  Vec3.prototype.matrixProduct = function(m){
  	
  	var x = m.value[0][0]*this.x + m.value[0][1]*this.y + m.value[0][2] * this.z;
    var y = m.value[1][0]*this.x + m.value[1][1]*this.y + m.value[1][2] * this.z;
    var z = m.value[2][0]*this.x + m.value[2][1]*this.y + m.value[2][2] * this.z;
    
    return new Vec3(x, y);
  }

  /* PUBLIC METHODS --------------------------------------------------*/
  function getIdentityMatrix(){
    return new Matrix3(
			[
      	[1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
      ]
    );
  }

  function createVectorFromPolar(angle, magnitude){
  	return new Vec3(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
  }

  // RETURN
  root.XVector = {
  	createVectorFromPolar: createVectorFromPolar,
    Vec3: Vec3,
    Matrix3: Matrix3,
    getIdentityMatrix: getIdentityMatrix
  };
})(this);